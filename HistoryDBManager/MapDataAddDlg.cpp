﻿#include "MapDataAddDlg.h"
#include "ui_MapDataAddDlg.h"
#include "DBManage.h"
#include <QFileDialog>
#include <QUuid>

MapDataAddDlg::MapDataAddDlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapDataAddDlg)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_ShowModal, true);

    m_mapID = "";
    m_isModify = false;
    m_imageData = QByteArray();

    connect(ui->pushButton_selectFile, SIGNAL(clicked()), this, SLOT(slotButtonClciked()));
    connect(ui->pushButton_preView, SIGNAL(clicked()), this, SLOT(slotButtonClciked()));
    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(slotButtonClciked()));
    connect(ui->pushButton_cancel, SIGNAL(clicked()), this, SLOT(slotButtonClciked()));

}

MapDataAddDlg::~MapDataAddDlg()
{
    delete ui;
}

void MapDataAddDlg::slotButtonClciked()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());
    if(btn == ui->pushButton_selectFile)
    {
        QString fileName = QFileDialog::getOpenFileName(this,
              tr("选择文件"), "", tr("Image Files (*.png *.jpg)"));
        fileName = (fileName == "") ? "fromSqlite" : fileName;
        ui->lineEdit_mapPath->setText(fileName);
    }
    else if(btn == ui->pushButton_preView)
    {
        if(ui->lineEdit_mapPath->text() == "fromSqlite")
        {
            QImage img = QImage::fromData(m_imageData);
            ui->widget_mapView->setQImage(img);
        }
        else
        {
            QString picPath = ui->lineEdit_mapPath->text();
            QPixmap img(picPath);
            ui->widget_mapView->setPixmap(img);
        }
    }
    else if(btn == ui->pushButton_save)
    {
        MapData data;
        data.mapID = m_mapID;
        data.dynastyID = ui->comboBox_dynasty->currentData().toString();
        data.mapArea = ui->lineEdit_area->text();
        data.mapNote = ui->lineEdit_note->text();
        data.mapPath = ui->lineEdit_mapPath->text();

        if(ui->lineEdit_mapPath->text() == "fromSqlite")
        {
            data.mapImage = m_imageData;
        }
        else
        {
            QFile file(data.mapPath);
            file.open(QIODevice::ReadOnly);
            QByteArray imageData = file.readAll();
            data.mapImage = imageData;
            file.close();
        }

        emit sigMapData(data, m_isModify);
        this->close();
    }
    else if(btn == ui->pushButton_cancel)
    {
        this->close();
    }
}

void MapDataAddDlg::updateDynastyItems()
{
    ui->comboBox_dynasty->blockSignals(true);
    ui->comboBox_dynasty->clear();

    // 朝代或时期下拉框
    QList<Dynasty> lstDynasty = DBManage::getInstance()->getDynastyDatas();
    for(int i = 0; i < lstDynasty.size(); ++i)
        ui->comboBox_dynasty->addItem(lstDynasty.at(i).dynastyName, lstDynasty.at(i).dynastyID);
    ui->comboBox_dynasty->setCurrentIndex(-1);
    ui->comboBox_dynasty->blockSignals(false);
}

void MapDataAddDlg::clearContents()
{
    m_isModify = false;
    ui->comboBox_dynasty->clear();
    ui->lineEdit_area->clear();
    ui->lineEdit_note->clear();
    ui->lineEdit_mapPath->clear();
    ui->widget_mapView->resetImage();

    m_mapID = QUuid::createUuid().toString();

    updateDynastyItems();


}

void MapDataAddDlg::setData(const MapData &data)
{
    updateDynastyItems();

    m_isModify = true;
    m_mapID = data.mapID;
    m_imageData = data.mapImage;
    int currentIndex = ui->comboBox_dynasty->findData(data.dynastyID);
    ui->comboBox_dynasty->setCurrentIndex(currentIndex);
    ui->lineEdit_area->setText(data.mapArea);
    ui->lineEdit_note->setText(data.mapNote);
    ui->lineEdit_mapPath->setText("fromSqlite");
}
