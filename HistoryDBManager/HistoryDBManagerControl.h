﻿#ifndef HISTORYDBMANAGERCONTROL_H
#define HISTORYDBMANAGERCONTROL_H

#include <QWidget>
#include "DBManagerIndex.h"
#include "DBManagerMain.h"
#include "HistoryMap.h"

class HistoryDBManagerControl : public QWidget
{
    Q_OBJECT

public:
    explicit HistoryDBManagerControl(QWidget *parent = nullptr);
    ~HistoryDBManagerControl();

private slots:
    void slotShowDBManagerPage();
    void slotShowIndexPage();
    void slotShowMapPage();
    void slotShowViewPage();

private:
    void hideAllWidget();

private:
    DBManagerIndex *m_dbManagerIndex;   // 首页
    DBManagerMain *m_dbManagerMain;     // 基础数据页
    HistoryMap *m_historyMap;           // 历史地图
};

#endif // HISTORYDBMANAGERCONTROL_H
