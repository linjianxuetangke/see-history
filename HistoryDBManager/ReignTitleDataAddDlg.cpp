﻿#include "ReignTitleDataAddDlg.h"
#include "ui_ReignTitleDataAddDlg.h"
#include <QUuid>
#include "DBManage.h"

ReignTitleDataAddDlg::ReignTitleDataAddDlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ReignTitleDataAddDlg)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_ShowModal, true);

    ui->spinBox_startTime->setRange(-9999, 9999);
    ui->spinBox_endTime->setRange(-9999, 9999);

    m_reignTitleID = "";
    m_isModify = false;

    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(slotSaveCountryData()));
    connect(ui->pushButton_cancel, SIGNAL(clicked()), this, SLOT(close()));

    ui->comboBox->setEnabled(true);
}

ReignTitleDataAddDlg::~ReignTitleDataAddDlg()
{
    delete ui;
}

void ReignTitleDataAddDlg::clearContents()
{
    m_isModify = false;
    ui->spinBox_startTime->setValue(0);
    ui->spinBox_endTime->setValue(0);
    ui->lineEdit_name->clear();
    updateCountryItems();

    m_reignTitleID = QUuid::createUuid().toString();
    ui->lineEdit_ReignTitleID->setText(m_reignTitleID);

}

void ReignTitleDataAddDlg::setData(const ReignTitle &data)
{
    m_isModify = true;
    updateCountryItems();

    ui->spinBox_startTime->setValue(data.startTime);
    ui->spinBox_endTime->setValue(data.endTime);
    ui->lineEdit_name->setText(data.reignTitleName);
    int currentIndex = ui->comboBox->findData(data.countryID);
    ui->comboBox->setCurrentIndex(currentIndex);

    m_reignTitleID = data.reignTitleID;
    ui->lineEdit_ReignTitleID->setText(m_reignTitleID);
}

void ReignTitleDataAddDlg::slotSaveCountryData()
{
    ReignTitle data;
    data.startTime = ui->spinBox_startTime->value();
    data.endTime = ui->spinBox_endTime->value();
    data.reignTitleName = ui->lineEdit_name->text();
    QString countryID = ui->comboBox->currentData().toString();
    data.countryID = countryID;
    data.reignTitleID = m_reignTitleID;

    emit sigReignTitleData(data, m_isModify);
    this->close();
}

void ReignTitleDataAddDlg::updateCountryItems()
{
    ui->comboBox->blockSignals(true);
    ui->comboBox->clear();
    m_lstCountry.clear();

    m_lstCountry = DBManage::getInstance()->getCountryDatas();
    for(int i = 0;  i < m_lstCountry.size(); ++i)
    {
        ui->comboBox->addItem(m_lstCountry[i].countryName, m_lstCountry[i].countryID);
    }
    ui->comboBox->setCurrentIndex(-1);
    ui->comboBox->blockSignals(false);
}




