#include "CommonTools.h"

// 提示信息
void CommonTools::showMessageBox(QString content)
{
    QMessageBox msgBox;
    msgBox.setText(content);
    msgBox.exec();
}

// 表格新增一行内容
void CommonTools::addTableWidgetRow(QTableWidget *table, QStringList lst)
{
    int rowCount = table->rowCount();
    table->setRowCount(rowCount + 1);
    int size = qMin(lst.size(), table->columnCount());
    for(int i = 0; i < size; ++i)
    {
        QTableWidgetItem *item = new QTableWidgetItem(lst[i]);
        item->setTextAlignment(Qt::AlignCenter);
        table->setItem(rowCount, i, item);
    }
}

// 表格更新一行内容
void CommonTools::updateTableWidgetRow(QTableWidget *table, int index, QStringList lst)
{
    if(index == -1)
        return;
    int size = qMin(lst.size(), table->columnCount());
    for(int i = 0; i < size; ++i)
    {
        table->item(index, i)->setText(lst[i]);
    }
}

void CommonTools::deleteTableWidgetItems(QTableWidget *table)
{
    int rowCount = table->rowCount();
    for(int i = rowCount - 1; i >= 0; --i)
    {
        table->removeRow(i);
    }
}
