﻿#include "DBManagerIndex.h"
#include "ui_DBManagerIndex.h"

DBManagerIndex::DBManagerIndex(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DBManagerIndex)
{
    ui->setupUi(this);
    connect(ui->pushButton_data, SIGNAL(clicked()), this, SIGNAL(sigShowData()));
    connect(ui->pushButton_historyMap, SIGNAL(clicked()), this, SIGNAL(sigShowMap()));
    connect(ui->pushButton_view, SIGNAL(clicked()), this, SIGNAL(sigShowView()));

}

DBManagerIndex::~DBManagerIndex()
{
    delete ui;
}
