﻿#ifndef MAPDATAADDDLG_H
#define MAPDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class MapDataAddDlg;
}

class MapDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit MapDataAddDlg(QWidget *parent = nullptr);
    ~MapDataAddDlg();

public:
    void clearContents();
    void setData(const MapData& data);

private slots:
    void slotButtonClciked();

private:
    void updateDynastyItems();

private:
    Ui::MapDataAddDlg *ui;
    QString m_mapID;
    QByteArray m_imageData;
    bool m_isModify;

signals:
    void sigMapData(MapData, bool);
};

#endif // MAPDATAADDDLG_H
