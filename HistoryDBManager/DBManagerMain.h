﻿#ifndef DBMANAGERMAIN_H
#define DBMANAGERMAIN_H

#include <QWidget>
#include <QTableWidget>
#include "HistoryData.h"

#include "DynastyDataAddDlg.h"
#include "CountryDataAddDlg.h"
#include "RulerDataAddDlg.h"
#include "CapitalDataAddDlg.h"
#include "ReignTitleDataAddDlg.h"
#include "MapDataAddDlg.h"

namespace Ui {
class DBManagerMain;
}

class DBManagerMain : public QWidget
{
    Q_OBJECT

public:
    explicit DBManagerMain(QWidget *parent = nullptr);
    ~DBManagerMain();

private slots:
    void slotButtonClicked();

    void slotDynastyDataAdd(Dynasty data, bool isModify);
    void slotCountryDataAdd(Country data, bool isModify);
    void slotRulerDataAdd(Ruler data, bool isModify);
    void slotCapitalDataAdd(Capital data, bool isModify);
    void slotReignTitleDataAdd(ReignTitle data, bool isModify);
    void slotMapDataAdd(MapData data, bool isModify);

    void slotMapTableWidgetClicked(QTableWidgetItem*);

private:
    void initData(bool isRefresh);  // 不刷新地图

    void addDynastyData(const Dynasty& data);
    void updateDynastyData(const Dynasty& data);

    void addCountryData(const Country& data);
    void updateCountryData(const Country& data);

    void addRulerData(const Ruler& data);
    void updateRulerData(const Ruler& data);

    void addCapitalData(const Capital& data);
    void updateCapitalData(const Capital& data);

    void addReignTitleData(const ReignTitle& data);
    void updateReignTitleData(const ReignTitle& data);

    void addMapData(const MapData& data);
    void updateMapData(const MapData& data);


private:
    Ui::DBManagerMain *ui;

    DynastyDataAddDlg *m_dynastyAdd;
    CountryDataAddDlg *m_countryAdd;
    RulerDataAddDlg *m_rulerAdd;
    CapitalDataAddDlg *m_capitalAdd;
    ReignTitleDataAddDlg *m_reignTitleAdd;
    MapDataAddDlg *m_mapAdd;

    int m_dynastyCurrentRow;
    int m_countryCurrentRow;
    int m_rulerCurrentRow;
    int m_capitalCurrentRow;
    int m_reignTitleCurrentRow;
    int m_mapCurrentRow;

signals:
    void sigReturn();
};

#endif // DBMANAGERMAIN_H
