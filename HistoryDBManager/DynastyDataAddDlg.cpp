﻿#include "DynastyDataAddDlg.h"
#include "ui_DynastyDataAddDlg.h"
#include <QUuid>

DynastyDataAddDlg::DynastyDataAddDlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DynastyDataAddDlg)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_ShowModal, true);

    ui->spinBox_startTime->setRange(-9999, 9999);
    ui->spinBox_endTime->setRange(-9999, 9999);

    m_dynastyID = "null";
    m_isModify = false;

    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(slotSaveDynastyData()));
    connect(ui->pushButton_cancel, SIGNAL(clicked()), this, SLOT(close()));
}

DynastyDataAddDlg::~DynastyDataAddDlg()
{
    delete ui;
}

void DynastyDataAddDlg::clearContents()
{
    ui->spinBox_startTime->setValue(0);
    ui->spinBox_endTime->setValue(0);
    ui->lineEdit_name->clear();
    m_isModify = false;
    m_dynastyID = "null";
}

void DynastyDataAddDlg::setData(const Dynasty &data)
{
    ui->spinBox_startTime->setValue(data.startTime);
    ui->spinBox_endTime->setValue(data.endTime);
    ui->lineEdit_name->setText(data.dynastyName);
    m_dynastyID = data.dynastyID;
    m_isModify = true;
}

void DynastyDataAddDlg::slotSaveDynastyData()
{
    Dynasty data;
    data.startTime = ui->spinBox_startTime->value();
    data.endTime = ui->spinBox_endTime->value();
    data.dynastyName = ui->lineEdit_name->text();
    if(m_isModify)
        data.dynastyID = m_dynastyID;
    else {
        data.dynastyID = QUuid::createUuid().toString();
    }
    emit sigDynastyData(data, m_isModify);
    this->close();
}
