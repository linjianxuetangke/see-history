﻿#ifndef COUNTRYDATAADDDLG_H
#define COUNTRYDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class CountryDataAddDlg;
}

class CountryDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit CountryDataAddDlg(QWidget *parent = nullptr);
    ~CountryDataAddDlg();

public:
    void clearContents();
    void setData(const Country& data);

private slots:
    void slotSaveCountryData();

private:
    Ui::CountryDataAddDlg *ui;
    QString m_countryID;
    bool m_isModify;

signals:
    void sigCountryData(Country, bool);
};

#endif // COUNTRYDATAADDDLG_H
