﻿#include "HistoryMap.h"
#include "ui_HistoryMap.h"
#include "DBManage.h"
#include <QDebug>

HistoryMap::HistoryMap(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::HistoryMap)
{
    ui->setupUi(this);

    connect(ui->btn_return, SIGNAL(clicked()), this, SIGNAL(sigReturn()));
    connect(ui->comboBox_dynasty, SIGNAL(currentIndexChanged(int)), this, SLOT(slotComboBoxIndexChangedDynasty(int)));
    connect(ui->comboBox_area, SIGNAL(currentIndexChanged(int)), this, SLOT(slotComboBoxIndexChangedArea(int)));
}

HistoryMap::~HistoryMap()
{
    delete ui;
}

void HistoryMap::initUI()
{
    ui->comboBox_dynasty->clear();
    ui->comboBox_area->clear();

    // 朝代或时期下拉框
    QList<Dynasty> lstDynasty = DBManage::getInstance()->getDynastyDatas();
    for(int i = 0; i < lstDynasty.size(); ++i)
        ui->comboBox_dynasty->addItem(lstDynasty.at(i).dynastyName, lstDynasty.at(i).dynastyID);
    ui->comboBox_dynasty->setCurrentIndex(0);

}

void HistoryMap::slotComboBoxIndexChangedDynasty(int index)
{
    if(index == -1)
        return;
    ui->comboBox_area->clear();
    ui->widget_mapView->resetImage();

    QString dynastyID = ui->comboBox_dynasty->itemData(index).toString();
//    qDebug() << "dynastyID = " << dynastyID;
    m_lstMapData = DBManage::getInstance()->getMapDatasByDynastyID(dynastyID);
    for(MapData data : m_lstMapData)
    {
        ui->comboBox_area->addItem(data.mapArea, QVariant::fromValue(data.mapID));
    }
    ui->comboBox_area->setCurrentIndex(0);
}

void HistoryMap::slotComboBoxIndexChangedArea(int index)
{
    if(index == -1)
        return;

    QString strMapID = ui->comboBox_area->itemData(index).toString();
    for(MapData data : m_lstMapData)
    {
        if(data.mapID == strMapID)
        {
            QImage img = QImage::fromData(data.mapImage);
            ui->widget_mapView->setQImage(img);
//            ui->widget_mapView->resetSize();
            ui->label_note->setText(data.mapNote);
        }
    }

#if 0
    QVariant currentData = ui->comboBox_area->itemData(index);
    MapData data;
    if (currentData.canConvert<MapData>())
        data = currentData.value<MapData>();
    ui->label_note->setText(data.mapNote);
    QImage img = QImage::fromData(data.mapImage);
    ui->widget_mapView->setQImage(img);
#endif
}
