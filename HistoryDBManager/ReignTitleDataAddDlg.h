﻿#ifndef REIGNTITLEDATAADDDLG_H
#define REIGNTITLEDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class ReignTitleDataAddDlg;
}

class ReignTitleDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit ReignTitleDataAddDlg(QWidget *parent = nullptr);
    ~ReignTitleDataAddDlg();

public:
    void clearContents();
    void setData(const ReignTitle& data);

private slots:
    void slotSaveCountryData();

private:
    void updateCountryItems();

private:
    Ui::ReignTitleDataAddDlg *ui;
    QString m_reignTitleID;
    bool m_isModify;
    QList<Country> m_lstCountry;

signals:
    void sigReignTitleData(ReignTitle, bool);
};

#endif // REIGNTITLEDATAADDDLG_H
