﻿#include "CountryDataAddDlg.h"
#include "ui_CountryDataAddDlg.h"
#include <QUuid>

CountryDataAddDlg::CountryDataAddDlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CountryDataAddDlg)
{
    ui->setupUi(this);
    this->setAttribute(Qt::WA_ShowModal, true);

    m_isModify = false;

    ui->spinBox_startTime->setRange(-9999, 9999);
    ui->spinBox_endTime->setRange(-9999, 9999);

    m_countryID = "";
    m_isModify = false;

    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(slotSaveCountryData()));
    connect(ui->pushButton_cancel, SIGNAL(clicked()), this, SLOT(close()));


}

CountryDataAddDlg::~CountryDataAddDlg()
{
    delete ui;
}

void CountryDataAddDlg::clearContents()
{
    m_isModify = false;
    ui->spinBox_startTime->setValue(0);
    ui->spinBox_endTime->setValue(0);
    ui->lineEdit_name->clear();
    m_countryID = QUuid::createUuid().toString();
    ui->lineEdit_id->setText(m_countryID);
}

void CountryDataAddDlg::setData(const Country &data)
{
    m_isModify = true;
    ui->spinBox_startTime->setValue(data.startTime);
    ui->spinBox_endTime->setValue(data.endTime);
    ui->lineEdit_name->setText(data.countryName);
    m_countryID = data.countryID;
    ui->lineEdit_id->setText(m_countryID);
}

void CountryDataAddDlg::slotSaveCountryData()
{
    Country data;
    data.startTime = ui->spinBox_startTime->value();
    data.endTime = ui->spinBox_endTime->value();
    data.countryName = ui->lineEdit_name->text();
    data.countryID = m_countryID;

    emit sigCountryData(data, m_isModify);
    this->close();
}



