#ifndef COMMONTOOLS_H
#define COMMONTOOLS_H

#include <QTableWidget>
#include <QMessageBox>
#include <QStringList>

class CommonTools
{
public:
    // 提示信息
    static void showMessageBox(QString content);

    // 表格新增一行内容
    static void addTableWidgetRow(QTableWidget *table, QStringList lst);

    // 表格更新一行内容
    static void updateTableWidgetRow(QTableWidget *table, int index, QStringList lst);

    // 清空表格
    static void deleteTableWidgetItems(QTableWidget* table);

};



#endif // COMMONTOOLS_H


