﻿#include "DBManagerMain.h"
#include "ui_DBManagerMain.h"
#include "DBManage.h"
#include "HistoryData.h"
#include "CommonTools.h"
#include <QMessageBox>
#include <QDebug>

DBManagerMain::DBManagerMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DBManagerMain)
{
    ui->setupUi(this);
    //    this->setWindowState(Qt::WindowMaximized);

    // 连接数据库
    DBManage::getInstance()->connectDB();

    // 返回首页
    connect(ui->btn_return, SIGNAL(clicked()), this, SIGNAL(sigReturn()));
    connect(ui->btn_selectDB, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_refresh, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 行颜色交替
    ui->tableWidget_dynasty->setAlternatingRowColors(true);
    ui->tableWidget_country->setAlternatingRowColors(true);
    ui->tableWidget_ruler->setAlternatingRowColors(true);
    ui->tableWidget_capital->setAlternatingRowColors(true);
    ui->tableWidget_reignTitle->setAlternatingRowColors(true);

    // 隐藏垂直表头,显示水平表头
    ui->tableWidget_dynasty->verticalHeader()->setVisible(false);
    ui->tableWidget_country->verticalHeader()->setVisible(false);
    ui->tableWidget_ruler->verticalHeader()->setVisible(false);
    ui->tableWidget_capital->verticalHeader()->setVisible(false);
    ui->tableWidget_reignTitle->verticalHeader()->setVisible(false);

    ui->tableWidget_dynasty->horizontalHeader()->setVisible(true);
    ui->tableWidget_country->horizontalHeader()->setVisible(true);
    ui->tableWidget_ruler->horizontalHeader()->setVisible(true);
    ui->tableWidget_capital->horizontalHeader()->setVisible(true);
    ui->tableWidget_reignTitle->horizontalHeader()->setVisible(true);

    // 朝代或时期
    m_dynastyCurrentRow = -1;
    m_dynastyAdd = new DynastyDataAddDlg();
    m_dynastyAdd->hide();
    connect(m_dynastyAdd, SIGNAL(sigDynastyData(Dynasty, bool)), this, SLOT(slotDynastyDataAdd(Dynasty, bool)));
    connect(ui->btn_add_dynasty, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_dynasty, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_dynasty, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 政权
    m_countryCurrentRow = -1;
    m_countryAdd = new CountryDataAddDlg();
    m_countryAdd->hide();
    connect(m_countryAdd, SIGNAL(sigCountryData(Country, bool)), this, SLOT(slotCountryDataAdd(Country, bool)));
    connect(ui->btn_add_country, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_country, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_country, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 统治者
    m_rulerCurrentRow = -1;
    m_rulerAdd = new RulerDataAddDlg();
    m_rulerAdd->hide();
    connect(m_rulerAdd, SIGNAL(sigRulerData(Ruler, bool)), this, SLOT(slotRulerDataAdd(Ruler, bool)));
    connect(ui->btn_add_ruler, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_ruler, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_ruler, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 都城
    m_capitalCurrentRow = -1;
    m_capitalAdd = new CapitalDataAddDlg();
    m_capitalAdd->hide();
    connect(m_capitalAdd, SIGNAL(sigCapitalData(Capital, bool)), this, SLOT(slotCapitalDataAdd(Capital, bool)));
    connect(ui->btn_add_capital, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_capital, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_capital, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 年号
    m_reignTitleAdd = new ReignTitleDataAddDlg();
    m_reignTitleAdd->hide();
    connect(m_reignTitleAdd, SIGNAL(sigReignTitleData(ReignTitle, bool)), this, SLOT(slotReignTitleDataAdd(ReignTitle, bool)));
    connect(ui->btn_add_reginTitle, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_reginTitle, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_reginTitle, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));

    // 地图
    m_mapCurrentRow = -1;
    m_mapAdd = new MapDataAddDlg();
    m_mapAdd->hide();
    connect(m_mapAdd, SIGNAL(sigMapData(MapData, bool)), this, SLOT(slotMapDataAdd(MapData, bool)));
    connect(ui->btn_add_map, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_modify_map, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->btn_delete_map, SIGNAL(clicked()), this, SLOT(slotButtonClicked()));
    connect(ui->tableWidget_map, SIGNAL(itemClicked(QTableWidgetItem *)), this, SLOT(slotMapTableWidgetClicked(QTableWidgetItem*)));

    // 初始化数据
    initData(false);
    ui->tabWidget->setCurrentIndex(0);


}

DBManagerMain::~DBManagerMain()
{
    delete ui;
}

void DBManagerMain::slotButtonClicked()
{
    QPushButton *btn = qobject_cast<QPushButton*>(sender());

    // ******************************************  朝代  ***********************************
    if(btn == ui->btn_add_dynasty)  // 朝代 新增
    {
        m_dynastyAdd->clearContents();
        m_dynastyAdd->show();
    }
    else if(btn == ui->btn_modify_dynasty)  // 朝代 更新
    {
        QTableWidgetItem* item = ui->tableWidget_dynasty->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_dynastyCurrentRow = ui->tableWidget_dynasty->currentRow();
        Dynasty data;
        data.startTime = ui->tableWidget_dynasty->item(m_dynastyCurrentRow, 0)->text().toInt();
        data.endTime = ui->tableWidget_dynasty->item(m_dynastyCurrentRow, 1)->text().toInt();
        data.dynastyName = ui->tableWidget_dynasty->item(m_dynastyCurrentRow, 2)->text();
        data.dynastyID = ui->tableWidget_dynasty->item(m_dynastyCurrentRow, 3)->text();

        m_dynastyAdd->setData(data);
        m_dynastyAdd->show();

        ui->tableWidget_dynasty->setCurrentItem(nullptr);
    }
    else if(btn == ui->btn_delete_dynasty)  // 朝代 删除
    {
        QTableWidgetItem* item = ui->tableWidget_dynasty->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_dynastyCurrentRow = ui->tableWidget_dynasty->currentRow();
        QString currentID = ui->tableWidget_dynasty->item(m_dynastyCurrentRow, 3)->text();
        bool result = DBManage::getInstance()->deleteDynastyData(currentID);
        if(result)
            ui->tableWidget_dynasty->removeRow(m_dynastyCurrentRow);
        else
        {
            CommonTools::showMessageBox("【朝代或时期】删除失败！");
        }
        ui->tableWidget_dynasty->setCurrentItem(nullptr);
    }

    // ******************************************  政权  ***********************************
    else if(btn == ui->btn_add_country)  // 政权 新增
    {
        m_countryAdd->clearContents();
        m_countryAdd->show();
    }
    else if(btn == ui->btn_modify_country)  // 政权 更新
    {
        QTableWidgetItem *item = ui->tableWidget_country->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }
        m_countryCurrentRow = ui->tableWidget_country->currentRow();
        Country data;
        data.startTime = ui->tableWidget_country->item(m_countryCurrentRow, 0)->text().toInt();
        data.endTime = ui->tableWidget_country->item(m_countryCurrentRow, 1)->text().toInt();
        data.countryName = ui->tableWidget_country->item(m_countryCurrentRow, 2)->text();
        data.countryID = ui->tableWidget_country->item(m_countryCurrentRow, 3)->text();

        m_countryAdd->setData(data);
        m_countryAdd->show();

        ui->tableWidget_country->setCurrentItem(nullptr);

    }
    else if(btn == ui->btn_delete_country)  // 政权 删除
    {
        QTableWidgetItem* item = ui->tableWidget_country->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_countryCurrentRow = ui->tableWidget_country->currentRow();
        QString currentID = ui->tableWidget_country->item(m_countryCurrentRow, 3)->text();
        bool result = DBManage::getInstance()->deleteCountryData(currentID);

        if(result)
            ui->tableWidget_country->removeRow(m_countryCurrentRow);
        else
        {
            CommonTools::showMessageBox("【政权】删除失败！");
        }
        ui->tableWidget_country->setCurrentItem(nullptr);
    }
    // ******************************************  统治者  ***********************************
    else if(btn == ui->btn_add_ruler)  // 统治者 新增
    {
        m_rulerAdd->clearContents();
        m_rulerAdd->show();
    }
    else if(btn == ui->btn_modify_ruler)  // 统治者 更新
    {
        QTableWidgetItem *item = ui->tableWidget_ruler->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }
        m_rulerCurrentRow = ui->tableWidget_ruler->currentRow();
        Ruler data;
        data.startTime = ui->tableWidget_ruler->item(m_rulerCurrentRow, 0)->text().toInt();
        data.endTime = ui->tableWidget_ruler->item(m_rulerCurrentRow, 1)->text().toInt();
        data.rulerName = ui->tableWidget_ruler->item(m_rulerCurrentRow, 2)->text();
        data.templeName = ui->tableWidget_ruler->item(m_rulerCurrentRow, 4)->text();
        data.posthumousName = ui->tableWidget_ruler->item(m_rulerCurrentRow, 5)->text();
        data.countryID = ui->tableWidget_ruler->item(m_rulerCurrentRow, 6)->text();
        data.rulerID = ui->tableWidget_ruler->item(m_rulerCurrentRow, 7)->text();

        m_rulerAdd->setData(data);
        m_rulerAdd->show();

        ui->tableWidget_ruler->setCurrentItem(nullptr);
     }
    else if(btn == ui->btn_delete_ruler)  // 统治者 删除
    {
        QTableWidgetItem* item = ui->tableWidget_ruler->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_rulerCurrentRow = ui->tableWidget_ruler->currentRow();
        QString currentID = ui->tableWidget_ruler->item(m_rulerCurrentRow, 4)->text();
        bool result = DBManage::getInstance()->deleteRulerData(currentID);
        if(result)
            ui->tableWidget_ruler->removeRow(m_rulerCurrentRow);
        else
        {
            CommonTools::showMessageBox("【统治者】删除失败！");
        }
        ui->tableWidget_ruler->setCurrentItem(nullptr);
    }

    // ******************************************  都城  ***********************************
    else if(btn == ui->btn_add_capital)  // 都城 新增
    {
         m_capitalAdd->clearContents();
        m_capitalAdd->show();
    }
    else if(btn == ui->btn_modify_capital)  // 都城 更新
    {
        QTableWidgetItem *item = ui->tableWidget_capital->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_capitalCurrentRow = ui->tableWidget_capital->currentRow();
        Capital data;
        data.startTime = ui->tableWidget_capital->item(m_capitalCurrentRow, 0)->text().toInt();
        data.endTime = ui->tableWidget_capital->item(m_capitalCurrentRow, 1)->text().toInt();
        data.capitalName = ui->tableWidget_capital->item(m_capitalCurrentRow, 2)->text();
        data.countryID = ui->tableWidget_capital->item(m_capitalCurrentRow, 4)->text();
        data.capitalID = ui->tableWidget_capital->item(m_capitalCurrentRow, 5)->text();

        m_capitalAdd->setData(data);
        m_capitalAdd->show();

        ui->tableWidget_capital->setCurrentItem(nullptr);
    }
    else if(btn == ui->btn_delete_capital)  // 都城 删除
    {
        QTableWidgetItem* item = ui->tableWidget_capital->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_capitalCurrentRow = ui->tableWidget_capital->currentRow();
        QString currentID = ui->tableWidget_capital->item(m_capitalCurrentRow, 4)->text();
        bool result = DBManage::getInstance()->deleteCapitalData(currentID);
        if(result)
            ui->tableWidget_capital->removeRow(m_capitalCurrentRow);
        else
        {
            CommonTools::showMessageBox("【都城】删除失败！");
        }
        ui->tableWidget_capital->setCurrentItem(nullptr);
    }

    // ******************************************  年号  ***********************************
    else if(btn == ui->btn_add_reginTitle)  // 年号 新增
    {
        m_reignTitleAdd->clearContents();
        m_reignTitleAdd->show();
    }
    else if(btn == ui->btn_modify_reginTitle)  // 年号 更新
    {
        QTableWidgetItem *item = ui->tableWidget_reignTitle->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }
        m_reignTitleCurrentRow = ui->tableWidget_reignTitle->currentRow();
        ReignTitle data;
        data.startTime = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 0)->text().toInt();
        data.endTime = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 1)->text().toInt();
        data.reignTitleName = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 2)->text();
        data.countryID = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 4)->text();
        data.reignTitleID = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 5)->text();

        m_reignTitleAdd->setData(data);
        m_reignTitleAdd->show();

        ui->tableWidget_reignTitle->setCurrentItem(nullptr);
    }
    else if(btn == ui->btn_delete_reginTitle)  // 年号 删除
    {
        QTableWidgetItem *item = ui->tableWidget_reignTitle->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }

        m_reignTitleCurrentRow = ui->tableWidget_reignTitle->currentRow();
        QString currentID = ui->tableWidget_reignTitle->item(m_reignTitleCurrentRow, 4)->text();
        bool result = DBManage::getInstance()->deleteReignTitleData(currentID);
        if(result)
            ui->tableWidget_reignTitle->removeRow(m_reignTitleCurrentRow);
        else
        {
            CommonTools::showMessageBox("【年号】删除失败！");
        }

        ui->tableWidget_reignTitle->setCurrentItem(nullptr);
    }

    // ******************************************  地图  ***********************************
    else if(btn == ui->btn_add_map)  // 地图 新增
    {
        m_mapAdd->clearContents();
        m_mapAdd->show();
    }
    else if(btn == ui->btn_modify_map)  // 地图 更新
    {
        QTableWidgetItem *item = ui->tableWidget_map->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }
        m_mapCurrentRow = ui->tableWidget_map->currentRow();
        QString mapId = ui->tableWidget_map->item(m_mapCurrentRow, 3)->text();
        MapData data = DBManage::getInstance()->getMapDataByID(mapId);
        m_mapAdd->setData(data);
        m_mapAdd->show();
        ui->tableWidget_map->setCurrentItem(nullptr);

#if 0
        MapData data;
        data.mapArea = ui->tableWidget_map->item(m_mapCurrentRow, 1)->text();
        data.mapNote = ui->tableWidget_map->item(m_mapCurrentRow, 2)->text();
        data.mapID = ui->tableWidget_map->item(m_mapCurrentRow, 3)->text();
        data.dynastyID = ui->tableWidget_map->item(m_mapCurrentRow, 4)->text();
#endif

    }
    else if(btn == ui->btn_delete_map)  // 地图 删除
    {
        QTableWidgetItem *item = ui->tableWidget_map->currentItem();
        if(item == nullptr)
        {
            CommonTools::showMessageBox("还没有选中行哦");
            return;
        }
        m_mapCurrentRow = ui->tableWidget_map->currentRow();
        QString currentID = ui->tableWidget_map->item(m_mapCurrentRow, 3)->text();
        bool result = DBManage::getInstance()->deleteMapData(currentID);
        if(result)
        {
            ui->tableWidget_map->removeRow(m_mapCurrentRow);
            ui->widget_preView->resetImage();
        }
        else
        {
            CommonTools::showMessageBox("【地图】删除失败！");
        }
        ui->tableWidget_map->setCurrentItem(nullptr);

    }

    // ******************************************  其他  ***********************************
    else if(btn == ui->btn_selectDB)  // 数据库 选择
    {
        CommonTools::showMessageBox("添砖加瓦中...");
    }
    else if(btn == ui->btn_refresh)  // 刷新
    {
        CommonTools::deleteTableWidgetItems(ui->tableWidget_dynasty);
        CommonTools::deleteTableWidgetItems(ui->tableWidget_country);
        CommonTools::deleteTableWidgetItems(ui->tableWidget_capital);
        CommonTools::deleteTableWidgetItems(ui->tableWidget_reignTitle);
        CommonTools::deleteTableWidgetItems(ui->tableWidget_ruler);
        initData(true);

    }
}

void DBManagerMain::slotDynastyDataAdd(Dynasty data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addDynastyData(data.startTime, data.endTime, data.dynastyName, data.dynastyID);
        if(result)
            addDynastyData(data);
        else
        {
            CommonTools::showMessageBox("【朝代或时期】保存失败！");
        }
    }
    else
    {
        bool result = DBManage::getInstance()->updateDynastyData(data.startTime, data.endTime, data.dynastyName, data.dynastyID);
        if(result)
            updateDynastyData(data);
        else
        {
            CommonTools::showMessageBox("【朝代或时期】更新失败！");
        }
    }
}

// 国家或政权
void DBManagerMain::slotCountryDataAdd(Country data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addCountryData(data);
        if(result)
            addCountryData(data);
        else
        {
            CommonTools::showMessageBox("【国家或政权】保存失败！");
        }
    }
    else
    {
        bool result = DBManage::getInstance()->updateCountryData(data);
        if(result)
            updateCountryData(data);
        else
            CommonTools::showMessageBox("【国家或政权】更新失败！");
    }
}

void DBManagerMain::slotRulerDataAdd(Ruler data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addRulerData(data);
        if(result)
        {
            addRulerData(data);
        }
        else
        {
            CommonTools::showMessageBox("【统治者】" + data.rulerName + "保存失败！");
        }
    }
    else
    {
        bool result = DBManage::getInstance()->updateRulerData(data);
        if(result)
        {
            updateRulerData(data);
        }
        else
        {
            CommonTools::showMessageBox("【统治者】" + data.rulerName + "保存失败！");
        }
    }
}

void DBManagerMain::slotCapitalDataAdd(Capital data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addCapitalData(data);
        if(result)
        {
            addCapitalData(data);
        }
        else
        {
            CommonTools::showMessageBox("【都城】" + data.capitalName + "保存失败！");
        }
    }
    else
    {
        bool result = DBManage::getInstance()->updateCapitalData(data);
        if(result)
        {
            updateCapitalData(data);
        }
        else
        {
            CommonTools::showMessageBox("【都城】" + data.capitalName + "更新失败！");
        }
    }
}

void DBManagerMain::slotReignTitleDataAdd(ReignTitle data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addReignTitleData(data);
        if(result)
        {
            addReignTitleData(data);
        }
        else
        {
            CommonTools::showMessageBox("【年号】" + data.reignTitleName + "保存失败！");
        }

    }
    else
    {
        bool result = DBManage::getInstance()->updateReignTitleData(data);
        if(result)
        {
            updateReignTitleData(data);
        }
        else
        {
            CommonTools::showMessageBox("【年号】" + data.reignTitleName + "保存失败！");
        }
    }
}

void DBManagerMain::slotMapDataAdd(MapData data, bool isModify)
{
    if(!isModify)
    {
        bool result = DBManage::getInstance()->addMapData(data);
        if(result)
        {
            addMapData(data);
        }
        else
        {
            CommonTools::showMessageBox("【地图】" + data.mapArea + "保存失败！");
        }

    }
    else
    {
        bool result = DBManage::getInstance()->updateMapData(data);
        if(result)
        {
            updateMapData(data);
        }
        else
        {
            CommonTools::showMessageBox("【地图】" + data.mapID + "更新失败！");
        }
    }

}

void DBManagerMain::slotMapTableWidgetClicked(QTableWidgetItem *item)
{
    if(item == nullptr)
        return;
    int row = item->row();
    QString mapId = ui->tableWidget_map->item(row, 3)->text();
    //    qDebug() << "mapId = " << mapId;
    MapData data = DBManage::getInstance()->getMapDataByID(mapId);
    QImage img = QImage::fromData(data.mapImage);
    ui->widget_preView->setQImage(img);
}

void DBManagerMain::initData(bool isRefresh)
{
    // 初始化朝代或时期表
    QList<Dynasty> lstDynasty = DBManage::getInstance()->getDynastyDatas();
    for(int i = 0; i < lstDynasty.size(); ++i)
        addDynastyData(lstDynasty.at(i));

    // 初始化政权表
    QList<Country> lstCountry = DBManage::getInstance()->getCountryDatas();
    for(int i = 0; i < lstCountry.size(); ++i)
        addCountryData(lstCountry.at(i));

    // 初始化统治者表
    QList<Ruler> lstRuler = DBManage::getInstance()->getRulerDatas();
    for(int i = 0; i < lstRuler.size(); ++i)
        addRulerData(lstRuler.at(i));

    // 初始化都城表
    QList<Capital> lstCapital = DBManage::getInstance()->getCapitalDatas();
    for(int i = 0; i < lstCapital.size(); ++i)
        addCapitalData(lstCapital.at(i));

    // 初始化年号表
    QList<ReignTitle> lstReignTitle = DBManage::getInstance()->getReignTitleDatas();
    for(int i = 0; i < lstReignTitle.size(); ++i)
        addReignTitleData(lstReignTitle.at(i));

    if(!isRefresh)
    {
        // 地图表
        QList<MapData> lstMapData = DBManage::getInstance()->getMapDatas();
        for(int i = 0; i < lstMapData.size(); ++i)
            addMapData(lstMapData.at(i));
    }


}

void DBManagerMain::addDynastyData(const Dynasty &data)
{
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.dynastyName << data.dynastyID;
    CommonTools::addTableWidgetRow(ui->tableWidget_dynasty, lst);
}

void DBManagerMain::updateDynastyData(const Dynasty &data)
{
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.dynastyName;
    CommonTools::updateTableWidgetRow(ui->tableWidget_dynasty, m_dynastyCurrentRow, lst);
}

void DBManagerMain::addCountryData(const Country &data)
{
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.countryName << data.countryID;
    CommonTools::addTableWidgetRow(ui->tableWidget_country, lst);
}

void DBManagerMain::updateCountryData(const Country &data)
{
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.countryName;
    CommonTools::updateTableWidgetRow(ui->tableWidget_country, m_countryCurrentRow, lst);
}

void DBManagerMain::addRulerData(const Ruler &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.rulerName << strCountryName << data.templeName << data.posthumousName << data.countryID << data.rulerID;
    CommonTools::addTableWidgetRow(ui->tableWidget_ruler, lst);
}

void DBManagerMain::updateRulerData(const Ruler &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.rulerName << strCountryName << data.templeName << data.posthumousName << data.countryID;
    CommonTools::updateTableWidgetRow(ui->tableWidget_ruler, m_rulerCurrentRow, lst);
}

void DBManagerMain::addCapitalData(const Capital &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.capitalName << strCountryName << data.countryID << data.capitalID;
    CommonTools::addTableWidgetRow(ui->tableWidget_capital, lst);
}

void DBManagerMain::updateCapitalData(const Capital &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.capitalName << strCountryName << data.countryID;
    CommonTools::updateTableWidgetRow(ui->tableWidget_capital, m_capitalCurrentRow, lst);
}

void DBManagerMain::addReignTitleData(const ReignTitle &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.reignTitleName << strCountryName << data.countryID << data.reignTitleID;
    CommonTools::addTableWidgetRow(ui->tableWidget_reignTitle, lst);
}

void DBManagerMain::updateReignTitleData(const ReignTitle &data)
{
    QString strCountryName = DBManage::getInstance()->getCountryNameByID(data.countryID);
    QStringList lst;
    lst << QString::number(data.startTime) << QString::number(data.endTime) << data.reignTitleName <<  strCountryName <<data.countryID;
    CommonTools::updateTableWidgetRow(ui->tableWidget_reignTitle, m_reignTitleCurrentRow, lst);
}

void DBManagerMain::addMapData(const MapData &data)
{
    int rowCount = ui->tableWidget_map->rowCount();
    QString strName = DBManage::getInstance()->getDynastyNameByID(data.dynastyID);

    QStringList lst;
    lst << strName << data.mapArea << data.mapNote << data.mapID << data.dynastyID;
    CommonTools::addTableWidgetRow(ui->tableWidget_map, lst);

    // 绑定对应图片
    ui->tableWidget_map->item(rowCount, 3)->setData(Qt::UserRole, data.mapImage);
}

void DBManagerMain::updateMapData(const MapData &data)
{
    QString strName = DBManage::getInstance()->getDynastyNameByID(data.dynastyID);
    QStringList lst;
    lst << strName << data.mapArea << data.mapNote << data.mapID;
    CommonTools::updateTableWidgetRow(ui->tableWidget_map, m_mapCurrentRow, lst);

    if(m_mapCurrentRow != -1)
        ui->tableWidget_map->item(m_mapCurrentRow, 3)->setData(Qt::UserRole, data.mapImage);
}












