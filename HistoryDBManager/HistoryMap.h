﻿#ifndef HISTORYMAP_H
#define HISTORYMAP_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class HistoryMap;
}

class HistoryMap : public QWidget
{
    Q_OBJECT

public:
    explicit HistoryMap(QWidget *parent = nullptr);
    ~HistoryMap();

public:
    void initUI();

private slots:
    void slotComboBoxIndexChangedDynasty(int index);
    void slotComboBoxIndexChangedArea(int index);

private:
    Ui::HistoryMap *ui;
    QList<MapData> m_lstMapData;

signals:
    void sigReturn();
};

#endif // HISTORYMAP_H
