﻿#ifndef MAPVIEWER_H
#define MAPVIEWER_H

#include <QObject>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QImage>
#include <QWheelEvent>
#include <QGraphicsScene>

class MapViewer : public QGraphicsView
{
    Q_OBJECT
public:
    MapViewer(QWidget *parent = nullptr);

public:
    void setQImage(const QImage& img);
    QImage getQImage();

    void setPixmap(const QPixmap& img);
    QPixmap getPixmap();

    void resetImage();

    void resetSize();

protected:
    virtual void wheelEvent(QWheelEvent *event) override;
    virtual void keyPressEvent(QKeyEvent *event) override;
    virtual void mousePressEvent(QMouseEvent *event) override;
    virtual void mouseMoveEvent(QMouseEvent *event) override;
    virtual void mouseReleaseEvent(QMouseEvent *event) override;
    void zoom(QPoint factor);
    void togglePan(bool pan, const QPoint &startPos = QPoint());
    void pan(const QPoint &panTo);
    void initShow();

private:
    QPixmap m_image;
    bool m_isPan;
    QPoint m_prevPan;
    QGraphicsScene *m_scene;

};

#endif // MAPVIEWER_H
