﻿#ifndef CAPITALDATAADDDLG_H
#define CAPITALDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class CapitalDataAddDlg;
}

class CapitalDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit CapitalDataAddDlg(QWidget *parent = nullptr);
    ~CapitalDataAddDlg();

public:
    void clearContents();
    void setData(const Capital& data);

private slots:
    void slotSaveCountryData();

private:
    void updateCountryItems();

private:
    Ui::CapitalDataAddDlg *ui;
    QString m_capitalID;
    bool m_isModify;
    QList<Country> m_lstCountry;

signals:
    void sigCapitalData(Capital, bool);
};

#endif // CAPITALDATAADDDLG_H
