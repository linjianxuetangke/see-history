﻿#ifndef DBMANAGERINDEX_H
#define DBMANAGERINDEX_H

#include <QWidget>


namespace Ui {
class DBManagerIndex;
}

class DBManagerIndex : public QWidget
{
    Q_OBJECT

public:
    explicit DBManagerIndex(QWidget *parent = nullptr);
    ~DBManagerIndex();

private:
    Ui::DBManagerIndex *ui;
signals:
    void sigShowData();
    void sigShowMap();
    void sigShowView();
};

#endif // DBMANAGERINDEX_H
