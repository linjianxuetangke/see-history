﻿#include "HistoryDBManagerControl.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HistoryDBManagerControl w;
    w.show();

    return a.exec();
}
