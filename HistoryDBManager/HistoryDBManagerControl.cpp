﻿#include "HistoryDBManagerControl.h"
#include <QHBoxLayout>
#include <QMessageBox>

HistoryDBManagerControl::HistoryDBManagerControl(QWidget *parent) :
    QWidget(parent)
{
    this->setWindowState(Qt::WindowMaximized);
    this->setWindowTitle("看见历史");

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setMargin(0);
    layout->setSpacing(0);

    // 首页
    m_dbManagerIndex = new DBManagerIndex(this);
    layout->addWidget(m_dbManagerIndex);
    m_dbManagerIndex->show();

    // 基础数据
    m_dbManagerMain = new DBManagerMain(this);
    layout->addWidget(m_dbManagerMain);
    m_dbManagerMain->hide();

    // 历史地图
    m_historyMap = new HistoryMap(this);
    layout->addWidget(m_historyMap);
    m_historyMap->hide();

    this->setLayout(layout);

    connect(m_dbManagerIndex, SIGNAL(sigShowData()), this, SLOT(slotShowDBManagerPage()));
    connect(m_dbManagerIndex, SIGNAL(sigShowMap()), this, SLOT(slotShowMapPage()));
    connect(m_dbManagerIndex, SIGNAL(sigShowView()), this, SLOT(slotShowViewPage()));

    connect(m_dbManagerMain, SIGNAL(sigReturn()), this, SLOT(slotShowIndexPage()));
    connect(m_historyMap, SIGNAL(sigReturn()), this, SLOT(slotShowIndexPage()));

}

HistoryDBManagerControl::~HistoryDBManagerControl()
{
    if(m_dbManagerMain != nullptr)
    {
        delete m_dbManagerMain;
        m_dbManagerMain = nullptr;
    }
    if(m_dbManagerIndex != nullptr)
    {
        delete m_dbManagerIndex;
        m_dbManagerIndex = nullptr;
    }
    if(m_historyMap != nullptr)
    {
        delete m_historyMap;
        m_historyMap = nullptr;
    }
}

void HistoryDBManagerControl::slotShowDBManagerPage()
{
    hideAllWidget();
    m_dbManagerMain->show();
}

void HistoryDBManagerControl::slotShowIndexPage()
{
    hideAllWidget();
    m_dbManagerIndex->show();
}

void HistoryDBManagerControl::slotShowMapPage()
{
    hideAllWidget();
    m_historyMap->initUI();
    m_historyMap->show();
}

void HistoryDBManagerControl::slotShowViewPage()
{
    QMessageBox msgBox;
    msgBox.setText("添砖加瓦中...");
    msgBox.exec();
}

void HistoryDBManagerControl::hideAllWidget()
{
    m_dbManagerIndex->hide();
    m_dbManagerMain->hide();
    m_historyMap->hide();
}
