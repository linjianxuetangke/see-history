﻿#ifndef DBMANAGE_H
#define DBMANAGE_H

#include <QObject>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

#include "HistoryData.h"

class DBManage : public QObject
{
    Q_OBJECT

public:
    static DBManage *getInstance();

public:
    bool connectDB();       // 连接数据库
    void setDatabase(QString dbPath);  // 设置数据库
    QString getCurrentDBName();  // 获取当前数据库名称
    bool exceSQL(QString sql);  // 执行SQL语句

public:
    // 朝代或时期
    bool addDynastyData(int startTime, int endTime, QString name, QString id);
    bool updateDynastyData(int startTime, int endTime, QString name, QString id);
    bool deleteDynastyData(QString id);
    QList<Dynasty> getDynastyDatas();
    QString getDynastyNameByID(const QString& id);

    // 政权
    bool addCountryData(const Country& data);
    bool updateCountryData(const Country& data);
    bool deleteCountryData(QString id);
    QList<Country> getCountryDatas();
    QString getCountryNameByID(const QString& id);

    // 统治者
    bool addRulerData(const Ruler& data);
    bool updateRulerData(const Ruler& data);
    bool deleteRulerData(QString id);
    QList<Ruler> getRulerDatas();

    // 都城
    bool addCapitalData(const Capital& data);
    bool updateCapitalData(const Capital& data);
    bool deleteCapitalData(QString id);
    QList<Capital> getCapitalDatas();

    // 年号
    bool addReignTitleData(const ReignTitle& data);
    bool updateReignTitleData(const ReignTitle& data);
    bool deleteReignTitleData(QString id);
    QList<ReignTitle> getReignTitleDatas();

    // 地图
    bool addMapData(const MapData& data);
    bool updateMapData(const MapData& data);
    bool deleteMapData(QString id);
    QList<MapData> getMapDatas();
    MapData getMapDataByID(const QString& id);
    QList<MapData> getMapDatasByDynastyID(const QString& id);

protected:
    DBManage();
    ~DBManage();

private:
    static DBManage *m_pInstance;
    QSqlDatabase m_database;
    QSqlQuery m_sql_query;

signals:
    void sigRefreshAllView();

};

#endif // DBMANAGE_H
