﻿#ifndef DYNASTYDATAADDDLG_H
#define DYNASTYDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class DynastyDataAddDlg;
}

class DynastyDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit DynastyDataAddDlg(QWidget *parent = nullptr);
    ~DynastyDataAddDlg();

public:
    void clearContents();
    void setData(const Dynasty& data);

private slots:
    void slotSaveDynastyData();

private:
    Ui::DynastyDataAddDlg *ui;
    QString m_dynastyID;
    bool m_isModify;

signals:
    void sigDynastyData(Dynasty, bool);
};

#endif // DYNASTYDATAADDDLG_H
