﻿#ifndef RULERDATAADDDLG_H
#define RULERDATAADDDLG_H

#include <QWidget>
#include "HistoryData.h"

namespace Ui {
class RulerDataAddDlg;
}

class RulerDataAddDlg : public QWidget
{
    Q_OBJECT

public:
    explicit RulerDataAddDlg(QWidget *parent = nullptr);
    ~RulerDataAddDlg();

public:
    void clearContents();
    void setData(const Ruler& data);

private slots:
    void slotSaveRulerData();

private:
    void updateCountryItems();

private:
    Ui::RulerDataAddDlg *ui;
    QString m_rulerID;
    bool m_isModify;
    QList<Country> m_lstCountry;

signals:
    void sigRulerData(Ruler, bool);
};

#endif // RULERDATAADDDLG_H
