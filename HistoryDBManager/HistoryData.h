﻿#ifndef HISTORYDATA_H
#define HISTORYDATA_H

#include <QString>
#include <QUuid>
#include <QImage>
#include <QMetaType>

struct Dynasty  // 朝代或时期
{
    QString dynastyID;
    int startTime;
    int endTime;
    QString dynastyName;
};
Q_DECLARE_METATYPE(Dynasty)

struct Country  // 政权
{
    QString countryID;
    int startTime;
    int endTime;
    QString countryName;
};
Q_DECLARE_METATYPE(Country)

struct Ruler  // 统治者
{
    QString rulerID;
    QString countryID;
    int startTime;
    int endTime;
    QString rulerName;
    QString templeName;  // 庙号
    QString posthumousName;  // 谥号
};
Q_DECLARE_METATYPE(Ruler)

struct Capital  // 都城
{
    QString capitalID;
    QString countryID;
    int startTime;
    int endTime;
    QString capitalName;
};
Q_DECLARE_METATYPE(Capital)

struct ReignTitle  // 年号
{
    QString reignTitleID;
    QString countryID;
    int startTime;
    int endTime;
    QString reignTitleName;
};
Q_DECLARE_METATYPE(ReignTitle)

struct MapData  // 地图
{
    QString mapID;
    QString dynastyID;
    QString mapArea;
    QString mapNote;
    QString mapPath;
    QByteArray mapImage;
};
Q_DECLARE_METATYPE(MapData)


#endif // HISTORYDATA_H
