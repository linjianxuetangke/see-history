# SeeHistory

## 介绍
看见历史

### 朝代或时期(Dynasty)

| 字段名         | 类型   | 字段含义 | 说明  |
| ----------- | ---- | ---- | --- |
| StartTime   | INT  | 开始时间 |     |
| EndTime     | INT  | 结束时间 |     |
| DynastyName | CHAR | 朝代名称 |     |
| DynastyID   | CHAR | 朝代id | 主键  |

```sql
CREATE TABLE [Dynasty](
  [StartTime] INT, 
  [EndTime] INT, 
  [DynastyName] CHAR, 
  [DynastyID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK);
```


### 政权(Country)

| 字段名         | 类型   | 字段含义 | 说明  |
| ----------- | ---- | ---- | --- |
| CountryID   | CHAR | 政权id | 主键  |
| StartTime   | INT  | 开始时间 |     |
| EndTime     | INT  | 结束时间 |     |
| CountryName | CHAR | 政权名称 |     |

```sql
CREATE TABLE [Country](
  [CountryID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
  [StartTime] INT, 
  [EndTime] INT, 
  [CountryName] CHAR);
```

### 统治者(Ruler)

| 字段名            | 类型   | 字段含义  | 说明                   |
| -------------- | ---- | ----- | -------------------- |
| RulerID        | CHAR | 统治者id | 主键                   |
| CountryID      | CHAR | 政权id  | 外键-Country表CountryID |
| RulerName      | CHAR | 统治者名称 |                      |
| StartTime      | INT  | 开始时间  |                      |
| EndTime        | INT  | 结束时间  |                      |
| TempleName     | CHAR | 庙号    |                      |
| PosthumousName | CHAR | 谥号    |                      |

```sql
CREATE TABLE [Ruler](
    [RulerID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [RulerName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT,
    [TempleName] CHAR,
    [PosthumousName] CHAR);
```

### 都城(Capital)

| 字段名         | 类型   | 字段含义 | 说明                   |
| ----------- | ---- | ---- | -------------------- |
| CapitalID   | CHAR | 都城id | 主键                   |
| CountryID   | CHAR | 政权id | 外键-Country表CountryID |
| CapitalName | CHAR | 都城名称 |                      |
| StartTime   | INT  | 开始时间 |                      |
| EndTime     | INT  | 结束时间 |                      |

```sql
CREATE TABLE [Capital](
    [CapitalID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK, 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [CapitalName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT);
```

### 年号(ReignTitle)

| 字段名            | 类型   | 字段含义 | 说明                   |
| -------------- | ---- | ---- | -------------------- |
| ReignTitleID   | CHAR | 年号id | 主键                   |
| CountryID      | CHAR | 政权id | 外键-Country表CountryID |
| ReignTitleName | CHAR | 年号名称 |                      |
| StartTime      | INT  | 开始时间 |                      |
| EndTime        | INT  | 结束时间 |                      |

```sql
CREATE TABLE [ReignTitle](
    [ReignTitleID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [ReignTitleName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT);
```

### 地图(Map)

| 字段名       | 类型   | 字段含义 | 说明                   |
| --------- | ---- | ---- | -------------------- |
| MapID     | CHAR | 地图id | 主键                   |
| DynastyID | CHAR | 政权id | 外键-Country表CountryID |
| MapArea   | CHAR | 地图区域 |                      |
| MapNote   | CHAR | 说明   |                      |
| MapImage  | BLOB | 地图数据 |                      |

```sql
CREATE TABLE [Map](
  [MapID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK, 
  [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
  [MapArea] CHAR, 
  [MapNote] CHAR, 
  [MapImage] BLOB);

```

### 批量建表

```sql
CREATE TABLE [Capital](
    [CapitalID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK, 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [CapitalName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT);

CREATE TABLE [Country](
  [CountryID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
  [StartTime] INT, 
  [EndTime] INT, 
  [CountryName] CHAR);

CREATE TABLE [Dynasty](
  [StartTime] INT, 
  [EndTime] INT, 
  [DynastyName] CHAR, 
  [DynastyID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK);

CREATE TABLE [Map](
  [MapID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK, 
  [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
  [MapArea] CHAR, 
  [MapNote] CHAR, 
  [MapImage] BLOB);

CREATE TABLE [ReignTitle](
    [ReignTitleID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [ReignTitleName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT);

CREATE TABLE [Ruler](
    [RulerID] CHAR PRIMARY KEY ON CONFLICT ROLLBACK), 
    [DynastyID] CHAR REFERENCES [Dynasty]([DynastyID]) ON DELETE CASCADE ON UPDATE CASCADE, 
    [RulerName] CHAR, 
    [StartTime] INT, 
    [EndTime] INT,
    [TempleName] CHAR,
    [PosthumousName] CHAR);


```